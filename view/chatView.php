<?php $title = $_SESSION['hostIp']; ?>

<?php ob_start(); ?>

<div class="w3-col m2">&nbsp;</div>
<div class="w3-container w3-row w3-col m8 w3-round" id="chatbox">
	<div id="chatRoom"><b class="w3-col m12">Chatroom #<?php echo $_SESSION['hostIp']; ?></b></div>
	<div id="msgHistory" class="w3-col m12"></div>
	<div class="w3-row w3-margin-right w3-col m12" id="bottombox">
		<textarea class="w3-col m12 w3-round" id="messageBox"></textarea>
		<span class="w3-col m4" id="userNick">Connected as <?php echo $_SESSION['nickname']; ?></span>
		<span class="w3-col m4">&nbsp</span>
		<a href="?a=logout" class="w3-col m4" id="logout">Log out</a>
	</div>
	
	<!-- Modal img -->
	<div id="imgModal" class="w3-modal" onclick="hideModal()">
		<div class="w3-modal-content">
			<img id="imgModalsrc" src="#" width="100%"/>
		</div>
	</div>
	<!-- Modal end -->
</div>

<script>
	function sendmsg(){
		var xhttp;
		var msgHistory = document.getElementById("msgHistory");
		var msg = document.getElementById("messageBox").value;
		
		xhttp = new XMLHttpRequest();
		document.getElementById("messageBox").value = "";
		xhttp.open("POST", "index.php", true);
		xhttp.onreadystatechange = function(){
			if(this.readyState == 4 && this.status == 200){
				console.log(this.responseText);
				scrollBottom();
			}
		}
		xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		xhttp.send("sendmsg="+encodeURIComponent(msg));

	}

	function imageZoom(element){
		document.getElementById('imgModal').style.display = 'block';
		document.getElementById('imgModalsrc').src = element.src;
	}

	function hideModal(){
		document.getElementById('imgModal').style.display='none';
	}
	
	function keydown(event){
		if (event.keyCode === 13 && !event.shiftKey){
			event.preventDefault();
				sendmsg();
		}
	}

	function scrollBottom(){
		var msgHistory = document.getElementById('msgHistory');
		msgHistory.scrollTop = msgHistory.scrollHeight-500;
	}

	function refreshChat(){
		var xhttp;
		var msgHistory = document.getElementById("msgHistory");
		
		xhttp = new XMLHttpRequest();
		xhttp.onreadystatechange = function(){
			if(this.readyState == 4 && this.status == 200){
				if(this.responseText != "" && this.responseText != 'disconnected'){
					if(msgHistory.scrollTop+500 != msgHistory.scrollHeight){
						msgHistory.innerHTML = this.responseText;
					}
					else{
						msgHistory.innerHTML = this.responseText;
						scrollBottom();
					}
				}
				if(this.responseText == 'disconnected'){
					window.location.href = 'index.php';
				}
			}
		}
		xhttp.open("POST", "index.php", true);
		xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		xhttp.send("refresh");
	}	

	document.getElementById('messageBox').onkeydown = keydown;

	setInterval(refreshChat,250);

</script>

<?php $content = ob_get_clean(); ?>

<?php require('template.php'); ?>