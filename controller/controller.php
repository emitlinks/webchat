<?php
require('controller/dbManager.php');
class chat{
	
	public function login($tempid){
		
		$dbManager = new DBManager();

		$_SESSION['tempid'] = $tempid;
		$_SESSION['nickname'] = $dbManager->getUserNickname($tempid);
		$_SESSION['roomid'] = $dbManager->getUserRoomId($tempid);
		$_SESSION['userid'] = $dbManager->getUserId($tempid);
		$_SESSION['msgCount'] = 0;
		
		if(isset($_SESSION['nickname'])){
			require('view\chatView.php');
		}else{
			//Delete the cookie if user isn't in database
			if(isset($_COOKIE['tempid'])){
				setcookie('tempid', null, time()-3600, null, null, false, true);
				echo "You aren't in the database anymore for some reason";
			}
			session_destroy();
			header("Location: index.php");
		}
	}

	public function logout($tempid, $autoLogout){
		$dbManager = new DBManager();
		
		setcookie('tempid', null, time()-3600, null, null, false, true);
		
		$dbManager->purgeUser($tempid);
		
		session_destroy();
		if($autoLogout){
			echo 'disconnected';
		}else{
			header("Location: index.php");
		}
	}

	public function createUser($hostIp, $nickname){
		$dbManager = new DBManager();
		
		//Check to see if the room exists and get it's id
		$roomid = $dbManager->getRoomIdByIp($hostIp);
		
		//Creating the room if it does not exists
		if(!isset($roomid)){
			$roomid = $dbManager->addRoom($hostIp);
		}
		
		//Purging old users so more nickname are available
		$dbManager->purgeOldUsers();
		
		//Does the nickname already exists
		$result = $dbManager->query("SELECT nickname FROM users WHERE roomid = '$roomid' AND nickname like '$nickname%' ORDER BY nickname ASC;");
		
		$i = 0;
		//Look for nickname starting the same as the one choosed
		while ($row = $result->fetch_assoc()){
			//Add one number as discriminator if the nickname exists already
			if($i > 0){
				$tempnick = $nickname.$i;
			}else{
				$tempnick = $nickname;
			}
			
			if($row['nickname'] == $tempnick){
				$i++;
			}
		}
		
		//Adding discriminator only if necessary
		if($i > 0){
			$nickname .= $i;
		}

		//Generating an unique ID
		$time = time()+3600;
		$newId = hash("sha256", $time.$hostIp.mt_rand(0,10000));
		
		//Creating a cookie to keep the user authentified
		setcookie('tempid', $newId, $time, null, null, false, true);
		$_SESSION['hostIp'] = $hostIp;
		
		$dbManager->addNewUser($nickname, $newId, $roomid);
		header("Location: index.php");
	}
	
	public function sendMessage($tempid, $message){
		$userid = $_SESSION['userid'];
		$roomid = $_SESSION['roomid'];
		$dbManager = new DBManager();
		$time = time()+3600;

		if(isset($tempid)){
			$dbManager->resetUserTimer($tempid);
			$message = htmlspecialchars($message, ENT_QUOTES);
			
			if($message != ""){
				$dbManager->addMessage($userid, $roomid, $message);
			}
		}
	}

	
	public function getMessages($tempid){
		if(isset($_SESSION['roomid'])){
			$dbManager = new DBManager();
			$roomid = $_SESSION['roomid'];
			$newMsgCount = $dbManager->getMessageCount($roomid);
			
			if($_SESSION['msgCount'] != $newMsgCount){
				$sql = "SELECT u.nickname, m.message, m.timestamp FROM messages m JOIN users u
						ON(m.userid = u.id OR m.userid = -1)
						WHERE u.roomid = $roomid OR u.roomid = -1 ORDER BY m.timestamp ASC;";
				$result = $dbManager->query($sql);

				$oldMsg = 0;
				$_SESSION['msgCount'] = $newMsgCount;
				
				$newMsgList = "";
				
				
				//Fetching message
				while($row = $result->fetch_assoc()){
					//Delete message if exists for 1 hour+
					if(time() > $row['timestamp']+3600){
						$oldMsg++;
					}
					else{
						//Replace breakline with <br>
						$message = nl2br($row['message']);
						
						//Italic
						if(preg_match('#\/\/(.+)\/\/#', $message, $ital)){
							$message = str_replace($ital[0], '<i>'.$ital[1].'</i>', $message);
						}
						//Bold
						if(preg_match('#\*(.+)\*#', $message, $bold)){
							$message = str_replace($bold[0], '<b>'.$bold[1].'</b>', $message);
						}
						//Links
						if(preg_match('#((https?|ftps?)://[^ <>]+)#', $message, $link)){
							$message = str_replace($link[0], '<a href="'.$link[1].'" target="_blank">'.$link[1].'</a>', $message);
						}
						
						//Formating the message
						echo "<div class=\"message\"><span class=\"nickname\"><b>".$row['nickname']."</b></span><br><div class=\"content\">".$message."</div>";
						
						
						//Adding images if links are provided
						if(preg_match('#((https?|ftps?):\/\/[^ ]+.(jpe?g|png|gif|bmp|PNG|GIF|JPE?G|BMP))#', $row['message'], $images)){
							echo "<img src='".$images[0]."' class=\"image\" onclick='imageZoom(this);' width='40%'></img>";
						}
						
						echo "</div>";
					}
				}
				
				return $newMsgList;
				
				//Deleting old messages if any
				if($oldMsg > 0){
					$dbManager->purgeOldMsg();
				}
			}
		}
	}
}