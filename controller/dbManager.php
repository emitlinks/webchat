<?php
class DBManager{
	
	private $dbServer = "127.0.0.1";
	private $dbUser = "chat";
	private $dbPass = "chat";
	private $dbName = "chat";
	
	private function dbConnect(){
		$db = new mysqli($this->dbServer, $this->dbUser, $this->dbPass, $this->dbName);

		if ($db->connect_error){
			die('Can\'t connect to DB');
		}
		else{
			return $db;
		}
	}
	
	public function query($sql){
		$db = $this->dbConnect();
		$result = $db->query($sql);
		$db->close();
		if($result === false){
			throw new Exception('Can\'t execute query');
		}
		return $result;
	}
	
	public function addNewUser($nickname, $tempid, $roomid){
		$time = time()+3600;
		$sql = "INSERT INTO users(nickname, tempid, roomid, time)
						VALUES('$nickname', '$tempid', '$roomid', '$time');";
		
		$db = $this->dbConnect();
		if($db->query($sql) === false){
			throw new Exception('Can\'t add new user');
		}
		$db->close();
	}

	public function addRoom($ip){
		$sql = "INSERT INTO rooms(ip) VALUES('$ip');";
		$db = $this->dbConnect();
		if($db->query($sql) === true){
			$db->close();
			return $this->getRoomIdByIp($ip);
		}
		else{
			$db->close();
			throw new Exception('Can\'t add new room');
		}
	}

	public function addMessage($userid, $roomid, $message){
		$time = time();
		$sql = "INSERT INTO messages(userid, message, timestamp, roomid)
					VALUES($userid, '$message', '$time', $roomid);";
		
		$db = $this->dbConnect();
		if($db->query($sql) === false){
			$db->close();
			throw new Exception('Can\'t add message');
		}
		$db->close();
	}

	public function getUserNickname($tempid){
		$sql = "SELECT nickname FROM users WHERE tempid = '$tempid';";
		$db = $this->dbConnect();
		$result = $db->query($sql);
		if($result->num_rows > 0){
			while($row = $result->fetch_assoc()){
				$username = $row['nickname'];
			}
			$db->close();
			return $username;
		}
		$db->close();
	}

	public function getUserId($tempid){
		$sql = "SELECT id FROM users WHERE tempid = '$tempid';";
		$db = $this->dbConnect();
		$result = $db->query($sql);
		
		if($result->num_rows > 0){
			while($row = $result->fetch_assoc()){
				$userid = $row['id'];
			}
			$db->close();
			return $userid;
		}
		$db->close();
		throw new Exception('Can\'t get user id');
	}

	public function getUserRoomId($tempid){
		$sql = "SELECT roomid FROM users WHERE tempid = '$tempid';";
		$db = $this->dbConnect();
		$result = $db->query($sql);
		
		if($result->num_rows > 0){
			while($row = $result->fetch_assoc()){
				$roomid = $row['roomid'];
			}
			$db->close();
			return $roomid;
		}
		$db->close();
	}

	public function getMessageCount($roomid){
		$sql = "SELECT count(*) as msgCount FROM messages WHERE roomid = $roomid;";
		$db = $this->dbConnect();
		$result = $db->query($sql);
		
		if($result->num_rows == 1){
			while($row = $result->fetch_assoc()){
				$db->close();
				return $row['msgCount'];
			} 
		}
		$db->close();
		throw new Exception('Can\'t get message count');
	}

	public function getRoomIdByIp($ip){
		$sql = "SELECT id FROM rooms WHERE ip = '$ip';";
		$db = $this->dbConnect();
		$result = $db->query($sql);
		
		if($result->num_rows == 1){
			while($row = $result->fetch_assoc()){
				$db->close();
				return $row['id'];
			}
		}
		$db->close();
	}


	public function resetUserTimer($tempid){
		$time = time()+3600;
		$sql = "UPDATE users SET time = $time WHERE tempid = '$tempid';";
		$db = $this->dbConnect();
		
		if($db->query($sql) === false){
			$db->close();
			throw new Exception('Can\'t reset user timer');
		}
		$db->close();
	}

	public function purgeUser($tempid){
		$sql = "SELECT id FROM users WHERE tempid = '$tempid'";
		$db = $this->dbConnect();
		$result = $db->query($sql);
		
		if($result->num_rows > 0){
			while($row = $result->fetch_assoc()){
				$userid = $row['id'];
				$deleteMsg = "DELETE FROM messages WHERE userid = $userid;";
				$deleteUsr = "DELETE FROM users WHERE id = $userid;";
				
				if($db->query($deleteMsg) === false){
					$db->close();
					throw new Exception('Can\'t delete user message');
				}
				if($db->query($deleteUsr) === false){
					$db->close();
					throw new Exception('Can\'t delete user');
				}
			}
		}
		$db->close();
	}

	public function purgeOldUsers(){
		$time = time();
		$sql = "SELECT id FROM users WHERE time < $time;";
		$db = $this->dbConnect();
		$result = $db->query($sql);
		
		if($result->num_rows > 0){
			while($row = $result->fetch_assoc()){
				$userid = $row['id'];
				$deleteMsg = "DELETE FROM messages WHERE userid = $userid;";
				$deleteUsr = "DELETE FROM users WHERE id = $userid;";
				
				if($db->query($deleteMsg) === false){
					$db->close();
					throw new Exception('Can\'t delete user message');
				}
				if($db->query($deleteUsr) === false){
					$db->close();
					throw new Exception('Can\'t delete user');
				}
			}
		}
		$db->close();
	}

	public function purgeOldMsg(){
		$time = time()-3600;
		$sql = "DELETE FROM messages WHERE timestamp < $time";
		$db = $this->dbConnect();
		
		if($db->query($sql) === false){
			$db->close();
			throw new Exception('Can\'t delete message');
		}
		$db->close();
	}
}