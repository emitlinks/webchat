<?php

require('controller/controller.php');

$chat = new chat();
session_start();

if(isset($_COOKIE['tempid'])){
	if(isset($_SESSION['tempid'])){
		//We have a cookie and a session so probably ok
		if(isset($_GET['a']) && $_GET['a'] == 'logout'){
			$chat->logout($_SESSION['tempid'], false);
		}elseif(isset($_POST['sendmsg'])){
			$message = $_POST['sendmsg'];
			$chat->sendMessage($_SESSION['tempid'], $message);
		}elseif(isset($_POST['refresh'])){
			$chat->getMessages($_SESSION['tempid']);
		}else{
			$chat->login($_SESSION['tempid']);
		}
	}else{
		$chat->login($_COOKIE['tempid']);
	}
}else{
	//No cookie so auto disconnect
	if(isset($_SESSION['tempid'])){
		$chat->logout($_SESSION['tempid'], true);
	}else{
		require('view\loginView.php');
		
		if(isset($_POST['newUser']) || (isset($_GET['a']) && $_GET['a'] == 'guest')){
			if(isset($_GET['a']) && $_GET['a'] == 'guest'){
				$hostIp = 'guest';
			}else{
				$hostIp = $_SERVER['REMOTE_ADDR'];
			}
			
			if(isset($_POST['nickname']) && $_POST['nickname'] != ''){
				$nickname = $_POST['nickname'];
			}else{
				$nickname = 'Bob';
			}
			
			$chat->createUser($hostIp, $nickname);
		}
	}
}